/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan5;

import java.util.Arrays;
import mpi.Datatype;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class LatihanMyBCast {

    void myBCast(Object data, int count, Datatype datatype, int root) {
        int worldRank;
        worldRank = MPI.COMM_WORLD.Rank();
        int worldSize;
        worldSize = MPI.COMM_WORLD.Size();

        if (worldRank == root) {
            int i;
            for (i = 0; i < worldSize; i++) {
                if (i != worldRank) {
                    MPI.COMM_WORLD.Send(data, 0, count, datatype, i, 0);
                }
            }
        } else {
            MPI.COMM_WORLD.Recv(data, 0, count, datatype, root, 0);
        }
    }    
    
    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 1;
        String data[] = new String[2];
        if (rank == root) {
            /*broadcast*/
            data[0] = "Hello";
            data[1] = "World";
        }

        /*before bcast*/
        System.out.println(rank + ", before: " + Arrays.toString(data));
        new LatihanMyBCast().myBCast(data, data.length, MPI.OBJECT, root);
        System.out.println(MPI.Wtick());
        System.out.println(MPI.Wtime());
        //1.543549981E9
        //1.543550044E9
        /*after bcast*/
        System.out.println(rank + ", after: " + Arrays.toString(data));
        MPI.Finalize();
    }

}