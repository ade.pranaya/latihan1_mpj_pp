/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan5;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class LatihanBCast {

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 1;
        String data[] = new String[2];
        if (rank == root) {
            /*broadcast*/
            data[0] = "Hello";
            data[1] = "World";
        }

        /*before bcast*/
        System.out.println(rank + ", before: " + Arrays.toString(data));
        MPI.COMM_WORLD.Bcast(data, 0, data.length, MPI.OBJECT, root);
        System.out.println(MPI.Wtick());
        System.out.println(MPI.Wtime());
        //1.543550001E9 4 PE
        //1.543550336E9 64 PE
        /*after bcast*/
        System.out.println(rank + ", after: " + Arrays.toString(data));
        MPI.Finalize();
    }

}
