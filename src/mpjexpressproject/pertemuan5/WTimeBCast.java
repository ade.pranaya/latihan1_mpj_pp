/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan5;

import java.util.Arrays;
import mpi.Datatype;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class WTimeBCast {

    void myBCast(Object data, int count, Datatype datatype, int root) {
        int worldRank;
        worldRank = MPI.COMM_WORLD.Rank();
        int worldSize;
        worldSize = MPI.COMM_WORLD.Size();

        if (worldRank == root) {
            int i;
            for (i = 0; i < worldSize; i++) {
                if (i != worldRank) {
                    MPI.COMM_WORLD.Send(data, 0, count, datatype, i, 0);
                }
            }
        } else {
            MPI.COMM_WORLD.Recv(data, 0, count, datatype, root, 0);
        }
    }

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 1;
        String data[] = new String[2];
        if (rank == root) {
            /*broadcast*/
            data[0] = "Hello";
            data[1] = "World";
        }

        double totalMyBCastTime = 0;
        double totalMPIBCastTime = 0;
        int numTrials = 10;
        for (int i = 0; i < numTrials; i++) {
            // Time my_bcast
            // Synchronize before starting timing
            MPI.COMM_WORLD.Barrier();
            totalMyBCastTime -= MPI.Wtime();
            new WTimeBCast().myBCast(data, data.length, MPI.OBJECT, root);
            // Synchronize again before obtaining final time
            MPI.COMM_WORLD.Barrier();
            totalMyBCastTime += MPI.Wtime();
//            System.out.println(i+": my bcast time: " + totalMyBCastTime);

            // Time MPI_Bcast
            MPI.COMM_WORLD.Barrier();
            totalMPIBCastTime -= MPI.Wtime();
            MPI.COMM_WORLD.Bcast(data, 0, data.length, MPI.OBJECT, root);
            MPI.COMM_WORLD.Barrier();
            totalMPIBCastTime += MPI.Wtime();

//            System.out.println(i+": my mpi time: " + totalMPIBCastTime);
        }
        System.out.println("my bcast time: " + totalMyBCastTime);
        System.out.println("my mpi time: " + totalMPIBCastTime);

        MPI.Finalize();
    }
}
