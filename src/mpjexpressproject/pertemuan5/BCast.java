/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan5;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 * @desc belajar Broad Cast
 */
public class BCast {

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;
        int data[] = new int[4];
        if (rank == 0) {
            /*broadcast*/
            data[0] = 10;
            data[1] = 1;
            data[2] = 2;
            data[3] = 3;
        }

        /*before bcast*/
        System.out.println(rank + ", before: " + Arrays.toString(data));
        MPI.COMM_WORLD.Bcast(data, 0, data.length, MPI.INT, root);

        /*after bcast*/
        System.out.println(rank + ", after: " + Arrays.toString(data));

        MPI.Finalize();
    }
}
