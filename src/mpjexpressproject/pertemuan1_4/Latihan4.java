/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan4 {

    public static void main(String[] args) {
        // kirim pesan point to point
        MPI.Init(args);
        int size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();

        //1) siapkan data
        int dataInt[] = {1, 2, 3, 4, 5, 6, 7, 8};
        char dataChar[] = {'a', 'd', 'e'};
        String dataString[] = {"ade", "pranaya"};

        //2) perintah send, pilih rank pengiris
        if (rank == 0) {
            for (int i = 1; i < size; i++) {
                MPI.COMM_WORLD.Send(dataInt, 0, dataInt.length, MPI.INT, i, i - 1);
                MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, i, i - 1);
                MPI.COMM_WORLD.Send(dataString, 0, dataString.length, MPI.OBJECT, i, i - 1);
            }
            /*
            Alur pengulangannya
            
            //send ke rank 1
            MPI.COMM_WORLD.Send(dataInt, 0, dataInt.length, MPI.INT, 1, 0);
            MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, 1, 0);

            //send ke rank 2
            MPI.COMM_WORLD.Send(dataInt, 0, dataInt.length, MPI.INT, 2, 1);
            MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, 2, 1);

            //send ke rank 3
            MPI.COMM_WORLD.Send(dataInt, 0, dataInt.length, MPI.INT, 3, 2);
            MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, 3, 2);
             */
        } //3) perintah receive, pilih rank penerima
        else {
            //4) menetapkan data penampung untuk penerimaan
            int dataRecvInt[] = new int[10];
            char dataRecvChar[] = new char[10];
            String dataRecvString[] = new String[2];
            MPI.COMM_WORLD.Recv(dataRecvInt, 0, dataRecvInt.length, MPI.INT, 0, rank - 1);
            MPI.COMM_WORLD.Recv(dataRecvChar, 0, dataRecvChar.length, MPI.CHAR, 0, rank - 1);
            MPI.COMM_WORLD.Recv(dataRecvString, 0, dataRecvString.length, MPI.OBJECT, 0, rank - 1);

            //5) tampilkan isi data yang diterima
            for (int i = 0; i < dataRecvString.length; i++) {
                System.out.println(dataRecvString[i]);
            }
            /*
            for (int i = 0; i < dataRecvChar.length; i++) {
                System.out.println(dataRecvChar[i]);
            }

            for (int i = 0; i < dataRecvInt.length; i++) {
                System.out.println(dataRecvInt[i]);
            }*/
        }
        MPI.Finalize();
    }

}
