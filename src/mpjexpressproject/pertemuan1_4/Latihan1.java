/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan1 {

    public static void main(String[] args) {
        MPI.Init(args);

        //Get the number of processor
        int world_size = MPI.COMM_WORLD.Size();

        //Get the rank of process
        /*
        nilai rank = 0 itu istilah nya master
        selain itu istilahnya slave 
         */
        int world_rank = MPI.COMM_WORLD.Rank();
        if (world_rank == 0) {
            System.out.println("rank of processor " + world_rank + " and i'm master");
            System.out.println("Number of processor " + world_size);
            System.out.println(MPI.Get_processor_name());
            System.out.println(MPI.Wtime());
            System.out.println(MPI.Wtick());
        } else {
            System.out.println("i'm slave from " + world_rank);
        }
        MPI.Finalize();
    }
}
