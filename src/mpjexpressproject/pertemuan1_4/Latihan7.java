/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan7 {

    public static void main(String[] args) {
        //pembagian data send and receive
        MPI.Init(args);
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int peRecv = size - 1;
        if (rank == 0) {
            int[] dataSend = new int[data.length / peRecv];
            for (int i = 1; i < size; i++) {
                int start = ((i - 1) * dataSend.length);
                System.arraycopy(data, start, dataSend, 0, dataSend.length);
                MPI.COMM_WORLD.Send(dataSend, 0, dataSend.length, MPI.INT, i, 0);
            }
        } else {
            int[] dataRecv = new int[data.length / peRecv];
            MPI.COMM_WORLD.Recv(dataRecv, 0, dataRecv.length, MPI.INT, 0, 0);
            System.out.println("Rank :" + rank);
            System.out.println(Arrays.toString(dataRecv));
        }
        MPI.Finalize();
    }
}
