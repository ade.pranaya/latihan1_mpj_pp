/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan2 {

    public static void main(String[] args) {
        MPI.Init(args);
        int world_size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();
        int a = 10, b = 100;
        float c;

        switch (rank) {
            case 0:
                c = a + b;
                break;
            case 1:
                c = a - b;
                break;
            case 2:
                c = (float)a / (float)b;
                break;
            case 3:
                c = a * b;
                break;
            default:
                throw new AssertionError();
        }
        System.out.println(rank + ";" + c);
        MPI.Finalize();
    }

}
