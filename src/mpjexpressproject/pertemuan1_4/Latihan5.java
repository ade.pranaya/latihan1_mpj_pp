/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan5 {

    public static void main(String[] args) {
        //MPI Ping Pong program
        MPI.Init(args);
        int ping_pong_count[] = {0};
        final int PING_PONG_LIMIT = 7;
        int world_rank = MPI.COMM_WORLD.Rank();
        int partner_rank = (world_rank + 1) % 2;
        
        while (ping_pong_count[0] < PING_PONG_LIMIT) {
            if (world_rank == ping_pong_count[0] % 2) {
                // Increment the ping pong count before you send it
                ping_pong_count[0]++;
                MPI.COMM_WORLD.Send(ping_pong_count, 0, ping_pong_count.length, MPI.INT, partner_rank, 0);
                System.out.println(world_rank + " sent and incremented ping_pong_count " + ping_pong_count[0] + " to " + partner_rank);
            } else {
                MPI.COMM_WORLD.Recv(ping_pong_count, 0, ping_pong_count.length, MPI.INT, partner_rank, 0);
                System.out.println(world_rank + " received ping_pong_count " + ping_pong_count[0] + " from " + partner_rank);
            }
        }
        
        MPI.Finalize();
    }
}
