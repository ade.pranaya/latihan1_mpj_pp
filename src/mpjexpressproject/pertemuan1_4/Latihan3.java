/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan3 {

    public static void main(String[] args) {
        //array
        MPI.Init(args);

        int[] a = {10, 20, 30, 40, 50, 60, 70, 80};
        int size = MPI.COMM_WORLD.Size();
        System.out.println("size: "+size);
        int rank = MPI.COMM_WORLD.Rank();

        System.out.println(rank + ";" + (a[rank * 2] + a[rank * 2 + 1]));

        /*PR terbesar di pemrograman paralel adalah mencari pola dari suatu perintah
        sehingga satu statement bisa dikerjakan & dimengerti oleh prosesors yang ada*/
        MPI.Finalize();
    }
}
