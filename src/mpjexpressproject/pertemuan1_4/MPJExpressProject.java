/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class MPJExpressProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MPI.Init(args);
        int me = MPI.COMM_WORLD.Rank(); //posisi komputer atau prosesor
        int size = MPI.COMM_WORLD.Size(); //jumlah prosesor
        if (me == 0) 
            System.out.println("Hi from <" + me + ">");
        else
            System.out.println("rank :"+me);
        MPI.Finalize();
    }

}
