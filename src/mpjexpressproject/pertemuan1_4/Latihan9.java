/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan9 {

    public static void main(String[] args) {
        //pembagian data send and receive dan menjumlahkannya
        //TUGAS FOUND
        MPI.Init(args);
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int peRecv = size - 1;
        if (rank == 0) {
            int[] dataSend = new int[data.length / peRecv];
            int[] dataX = {10};
            for (int i = 1; i < size; i++) {
                int start = ((i - 1) * dataSend.length);
                System.arraycopy(data, start, dataSend, 0, dataSend.length);
                MPI.COMM_WORLD.Send(dataSend, 0, dataSend.length, MPI.INT, i, 0);
                MPI.COMM_WORLD.Send(dataX, 0, dataX.length, MPI.INT, i, 1);
            }
            boolean[] foundRecv = new boolean[1];
            int[] iFoundRecv = new int[1];
            for (int i = 1; i < size; i++) {
                MPI.COMM_WORLD.Recv(foundRecv, 0, foundRecv.length, MPI.BOOLEAN, i, 0);
                if (foundRecv[0]) {
                    System.out.println("data "+dataX[0]+" ketemu ");
                }
            }
        } else {
            int[] dataRecv = new int[data.length / peRecv];
            int[] dataXRecv = new int[1];
            MPI.COMM_WORLD.Recv(dataRecv, 0, dataRecv.length, MPI.INT, 0, 0);
            MPI.COMM_WORLD.Recv(dataXRecv, 0, dataXRecv.length, MPI.INT, 0, 1);
            boolean[] found = new boolean[1];
            for (int i = 0; i < dataRecv.length; i++) {
                if (dataXRecv[0] == dataRecv[i]) {
                    found[0] = true;
                    break;
                }
            }
            MPI.COMM_WORLD.Send(found, 0, found.length, MPI.BOOLEAN, 0, 0);
        }
        MPI.Finalize();
    }
}
