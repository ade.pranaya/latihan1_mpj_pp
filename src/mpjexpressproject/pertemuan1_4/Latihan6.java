/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan1_4;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class Latihan6 {

    public static void main(String[] args) {
        MPI.Init(args);
        int token[] = new int[1];
        int world_rank = MPI.COMM_WORLD.Rank();
        int world_size = MPI.COMM_WORLD.Size();

        if (world_rank != 0) {
            MPI.COMM_WORLD.Recv(token, 0, 1, MPI.INT, world_rank - 1, 0);
            System.out.println("Process " + world_rank + " received token " + token[0] + " from process " + (world_rank - 1));
        } else {
            // Set the token's value if you are process 0
            token[0] = -1;
        }

        MPI.COMM_WORLD.Send(token, 0, 1, MPI.INT, ((world_rank + 1) % world_size), 0);
// Now process 0 can receive from the last process.
        if (world_rank == 0) {
MPI.COMM_WORLD.Recv(token, 0, 1, MPI.INT, (world_size-1), 0);
            System.out.println("Process " + world_rank + " received token " + token[0] + " from process " + (world_size - 1));
        }
        MPI.Finalize();
    }

}
