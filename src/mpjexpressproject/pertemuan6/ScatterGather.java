/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6;

import mpi.MPI;

/**
 *
 * @author depran
 */
public class ScatterGather {

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;
        
        /* scatter */
        int arrSize = 8;
        int data[] = new int[arrSize];
        if (rank == root) {
            for (int i = 0; i < data.length; i++) {
                data[i] = i + 1;
//                data[i] = (int) (Math.random()*10)+1;
            }
        }

        int bufSize = (int) data.length/ size;
        int recvData[] = new int[bufSize];
        for (int i = 0; i < data.length; i++) {
            System.out.println("Before scatter " + rank + ": " + data[i]);
        }

        MPI.COMM_WORLD.Scatter(data, 0, bufSize, MPI.INT, recvData, 0, bufSize, MPI.INT, root);

        for (int i = 0; i < recvData.length; i++) {
            System.out.println("After scatter rank " + rank + ": " + recvData[i]);
        }

        /*  local processing: hitung average */
        int total = 0;
        for (int i = 0; i < bufSize; i++) {
            total = total + recvData[i];
        }

        float avg = (float) total / bufSize;
        System.out.println(rank + "; Rata-rata: " + avg);

        /* end of local processing
        *
        *gather 
        **/
        int bufAvgSize = 1;
        float[] bufAvg = new float[bufAvgSize];
        bufAvg[0] = avg;

        float bufAvgTotal[] = new float[bufAvgSize * size];
        MPI.COMM_WORLD.Gather(bufAvg, 0, bufAvgSize, MPI.FLOAT, bufAvgTotal, 0, bufAvgSize, MPI.FLOAT, root);
        float avgTotal = 0;

        if (rank == 0) {
            for (int i = 0; i < bufAvgTotal.length; i++) {
//            System.out.println("After gather " + rank + "; " + bufAvgTotal[i]);
                avgTotal = avgTotal + bufAvgTotal[i];
            }
        }
        System.out.println(rank + "; avgTotal = " + avgTotal / size);

        MPI.Finalize();
    }

}
