/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class ReduceMax {

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;
        int data[] = new int[2];

        switch (rank) {
            case 0:
                data[0] = 5;
                data[1] = 1;
                break;
            case 1:
                data[0] = 2;
                data[1] = 3;
                break;
            case 2:
                data[0] = 7;
                data[1] = 8;
                break;
            default:
                data[0] = 4;
                data[1] = 2;
        }
        
        System.out.println("Before rank: " + rank + " " + Arrays.toString(data));

        int dataRecv[] = new int[data.length];

        MPI.COMM_WORLD.Reduce(data, 0, dataRecv, 0, dataRecv.length, MPI.INT, MPI.MAX, root);
        if (rank == 0) {
            System.out.println("After rank: " + rank + " " + Arrays.toString(dataRecv));
        }
        MPI.Finalize();
    }
}
