/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class ScatterGather2 {

    public static void main(String[] args) {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;

        /* scatter */
        int arrSize = 8;
        int data[] = new int[arrSize];
        if (rank == root) {
            for (int i = 0; i < data.length; i++) {
                data[i] = (int) (Math.random() * 10) + 1;
            }
        }

        int bufSize = (int) Math.floorDiv(data.length, size);
        int recvData[] = new int[bufSize];

        System.out.println("Before scatter rank " + rank + ": " + Arrays.toString(data));
        MPI.COMM_WORLD.Scatter(data, 0, bufSize, MPI.INT, recvData, 0, bufSize, MPI.INT, root);

        /*  local processing: sort */
        Arrays.sort(recvData);
        /* end of local processing */

        System.out.println("After scatter and sort rank " + rank + ": " + Arrays.toString(recvData));

        /* gather */
        int bufRecvData[] = new int[recvData.length * size];
        MPI.COMM_WORLD.Gather(recvData, 0, recvData.length, MPI.INT, bufRecvData, 0, recvData.length, MPI.INT, root);

        if (rank == 0) {
            System.out.println("terutut parsial" + Arrays.toString(bufRecvData));
            Arrays.sort(bufRecvData);
            System.out.println("terutut total" + Arrays.toString(bufRecvData));
        }

        System.out.println("rank :"+rank+": "+MPI.Wtime());
        MPI.Finalize();
    }
}
