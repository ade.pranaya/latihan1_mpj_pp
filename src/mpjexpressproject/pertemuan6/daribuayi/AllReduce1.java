/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6.daribuayi;


import java.util.Random;
import mpi.MPI;

/**
 *
 * @author pbasari
 */
public class AllReduce1 {
    public static void main (String args[]) {
        MPI.Init(args);
        int size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();
        int root = 0;
        
        /* create random */
         Random rnd = new Random();
        int max = 100;
        
        int arrSize = 8;
        int[] data = new int[arrSize];
        for (int i = 0; i < arrSize; i ++) {
            data[i] = rnd.nextInt(max);
        }
            
        // cek isi array 
        for (int i = 0; i < arrSize; i ++) {
            System.out.println(rank + "; " +data[i]);
        }
        
        // Sum the numbers locally
        float localSum = 0;
        for (int i = 0; i < data.length; i++) {
            localSum += data[i];
        }
        
        // cek sum 
        System.out.println(rank + "; Local sum = " +localSum + "; Average = "+ localSum/data.length);
       
        
        // Reduce all of the local sums into the global sum
        // global sum
        float [] sendBuf = new float[1];
        sendBuf[0] = localSum;
        
        float globalSum = 0;
        float[] recvBuf = new float[1];
        
        MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.SUM, root);
       
        globalSum = recvBuf[0];
        if (rank == 0)
            System.out.println(rank + "; Global sum = " + globalSum + "; Average = "+ globalSum/data.length);
       
        MPI.Finalize();
    }
}
