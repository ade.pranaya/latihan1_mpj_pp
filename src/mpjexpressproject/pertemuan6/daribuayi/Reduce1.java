/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6.daribuayi;

import java.util.Random;
import mpi.MPI;

/**
 *
 * @author pbasari
 */
public class Reduce1 {
    public static void main (String args[]) {
        MPI.Init(args);
        int size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();
        int root = 0;

        /* create random array */
        Random rnd = new Random();
        int max = 100;
        
        int arrSize = 8;
        int[] data = new int[arrSize];
        //if (rank == root) {
            for (int i = 0; i < arrSize; i ++) {
                data[i] = rnd.nextInt(max);
            }
            
            // cek isi array 
            for (int i = 0; i < arrSize; i ++) {
                System.out.println(rank + "; "+ data[i]);
            }
        //}
        
        /* sending to all processes */
        int bufSize = data.length/size;
        int bufData [] = new int[bufSize];
        
        for (int i = 0; i < data.length; i ++) {
            //System.out.println("Before scatter "+ rank + "; " + data[i]);
        }
        
        //MPI.COMM_WORLD.Scatter(data, 0, bufSize, MPI.INT, bufData, 0, bufSize, MPI.INT, root);
        
        for (int i = 0; i < bufData.length; i ++) {
            //System.out.println("After scatter " + rank + "; " + bufData[i]);
        }
       
        // Sum the numbers locally
        float localSum = 0;
        for (int i = 0; i < bufData.length; i++) {
            //localSum += bufData[i];
        }
        
        for (int i = 0; i < data.length; i++) {
            localSum += data[i];
        }
        
        // cek sum 
        System.out.println(rank + "; Local sum = " +localSum + "; Average = "+ localSum/data.length);
        
        // global sum
        float [] sendBuf = new float[bufSize];
        sendBuf[0] = localSum;
        
        float globalSum = 0;
        float[] recvBuf = new float[data.length];
        //MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.SUM, root);
        MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.SUM, root);
        
        /* cek isi recvBuf */
        for (int i = 0; i < recvBuf.length; i ++) {
            System.out.println("After reduce " + rank + "; " + recvBuf[i]);
        }
        
        globalSum = recvBuf[0];    
        // Print the result
        if (rank == root) {
            System.out.println("Total sum = " + globalSum +" Average = " + globalSum/data.length);
        }
        
        MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.MAX, root);
        if (rank == root) {
            System.out.println("Max = " + recvBuf[0]);
        }
        
        MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.MIN, root);
        if (rank == root) {
            System.out.println("Min = " + recvBuf[0]);
        }
        
        MPI.COMM_WORLD.Reduce(sendBuf, 0, recvBuf, 0, 1, MPI.FLOAT, MPI.PROD, root);
        if (rank == root) {
            System.out.println("Production = " + recvBuf[0]);
        }
        MPI.Finalize();
    }
}
