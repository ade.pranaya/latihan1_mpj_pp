/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6.daribuayi;


import mpi.MPI;
import java.util.*;
/**
 *
 * @author pbasari
 */
public class ScatterGather {
    public static void main (String args[]) {
                
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;
        
        /* scatter */
        int arrSize = 20;
        int[] data = new int[arrSize];
        if (rank == root) {
            for (int i = 0; i < arrSize; i ++) {
                data[i] = i;
            }
        }
        
        int bufSize = data.length/size;
        int recvData [] = new int[bufSize];
        
        for (int i = 0; i < data.length; i ++) {
            //System.out.println("Before scatter "+ rank + "; " + data[i]);
        }
        
        MPI.COMM_WORLD.Scatter(data, 0, bufSize, MPI.INT, 
                              recvData, 0, bufSize, MPI.INT, root);
        
        for (int i = 0; i < recvData.length; i ++) {
            //System.out.println("After scatter " + rank + "; " + recvData[i]);
        }
        /* local processing: hitung average */
        int total = 0;
        for (int i = 0; i < bufSize; i++) {
            total = total + recvData[i];
        
        }
        int avg = total/bufSize;
        System.out.println(rank + "; Rata-rata: " +avg);
        
        /* end of local processing */
    
        /* gather */
        int bufAvgSize = 1;
        int [] bufAvg = new int[bufAvgSize];
        bufAvg [0] = avg;
        
        int bufAvgTotal [] = new int[bufAvgSize*size];
        System.out.println("sebelum"+Arrays.toString(bufAvg));
        MPI.COMM_WORLD.Gather(bufAvg, 0, bufAvgSize, MPI.INT, 
                bufAvgTotal, 0, bufAvgSize, MPI.INT, root);
        System.out.println("sesudah"+Arrays.toString(bufAvgTotal));
        int avgTotal = 0;
        for (int i = 0; i < bufAvgTotal.length; i ++) {
            if (rank == 0) {
                System.out.println("After gather " + rank + "; " + bufAvgTotal[i]);
                avgTotal = avgTotal + bufAvgTotal[i];
            }
        }
        System.out.println(rank + "; avgTotal = " +avgTotal/size);
        
        MPI.Finalize();
    }
}
