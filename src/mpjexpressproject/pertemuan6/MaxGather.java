/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjexpressproject.pertemuan6;

import java.util.Arrays;
import mpi.MPI;

/**
 *
 * @author depran
 */
public class MaxGather {

    public static void main(String[] args) {
        //latihan 2
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        int root = 0;
        int data[] = new int[2];

        /* set data awal ke setiap PE */
        switch (rank) {
            case 0:
                data[0] = 5;
                data[1] = 1;
                break;
            case 1:
                data[0] = 2;
                data[1] = 3;
                break;
            case 2:
                data[0] = 7;
                data[1] = 8;
                break;
            default:
                data[0] = 4;
                data[1] = 2;
        }
        /* menyimpan data maksimal disetiap PE kedalam var dataMaxLocal */
        int dataMaxLocal[] = new int[1];
        dataMaxLocal[0] = Arrays.stream(data).max().getAsInt();

        /* tampil semua data pada PE sebelum dilakukan reduce */
        System.out.println("Before rank: " + rank + " " + Arrays.toString(dataMaxLocal));

        int dataRecv[] = new int[dataMaxLocal.length * size];

        /* 
        * mengumpulkan data dari semua PE ke root
        **/
        MPI.COMM_WORLD.Gather(dataMaxLocal, 0, dataMaxLocal.length, MPI.INT, dataRecv, 0, dataMaxLocal.length, MPI.INT, root);

        /* tampil semua data pada PE 0 (root) setelah dilakukan reduce */
        if (rank == 0) {
            System.out.println("After rank: " + rank + " " + Arrays.toString(dataRecv));
        }
        MPI.Finalize();
    }
}
